## Social Justice Research <br> & <br> Social Media

---
@snap[north span-100]
## Dataset and Purpose <br><br>

Use twitter data to highlight opinions and sentiment towards Voter-ID Laws in North Carolina and their disproportionate impact on poor and minority communities. It is important to understand the sentiments in order to asses the effectiveness of campaigns to highlight the social justice impact of these laws.
@snapend


---

@snap[north span-100]
## Issue <br>

Voter-ID Laws are being considered in North Carolina as a preventive measure against Voter Fraud.

These laws disproportionately impact poorer communities in a few ways:
@snapend

@snap[south-west span-30]
@box[bg-green text-white rounded fragment](Cost # Records for Proof of Identity typically cost money )
@snapend


@snap[south span-30]
@box[bg-green text-white rounded fragment](Time # Many must take significant time off of work to obtain a State ID)
@snapend

@snap[south-east span-30]
@box[bg-green text-white rounded fragment](Access # Many must travel significant distances to obtain an ID)
@snapend


---

@snap[north span-100]
## Status ##
@snapend

@ul
- Passed as State Constitutional Amendment in 2018
- Blocked by Federal District Judge Jan 2020
- Blocked by State Appeals court in Feb 2020
@ulend

---

@snap[north span-100]

### **Ethics, Privacy and Profiling**
<br><br>
Collecting data for this type of research has Ethical, Privacy and Social Justice considerations to take into account. It is important that we do not exacerbate any current social biases while we are trying to research ways to fix another.
@snapend

---
@snap[north span-100]

### **Ethics, Privacy and Profiling**
@snapend


@snap[south span-85]
![](assets/img/mindmap.svg)
@snapend
---

@snap[north span-100]
## Summary


<br>
### Social Justice Consideration ###
<br>
This disproportionate impact combined with the unproven risk of Voter Fraud amounts to **voter suppression**. Understanding the opinions and motivations presented on social media can help us to effectively message the social justice impacts of Voter ID laws.

@snapend
